CLUBS = []
with open("clubs.txt", "r") as fh:
    CLUBS = [l.strip() for l in fh] + ["Other Club"]


class HTML(object):
    def __init__(self):
        self.out = [
            "---",
            "title: Full Conference Registration Form",
            "draft: false",
            "---",
            "",
            '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>',
            '<script src="/js/reg_form.js"></script>',
            '<form name="registration" method="POST" data-netlify="true" action="/registration_result">',
        ]
        self.level = 1
        self.out.append(
            f'{"~" * self.level}The Council of Governors has been able to subsidize the registration fee by R250 per person for full registrations - the registration fee for the 2023 MD Convention is <font style="color:#E1CC00;text-decoration: underline">R1300</font> per attendee.'
        )
        self.out.append(f'{"~" * self.level}<ul>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<li>The details of your registration will be sent to you, including a unique registration number. Payment details will also be included in that email. Please use your registration number as a reference when paying. Payment should be received by 10 April 2023.</li>'
        )

        self.out.append(
            f'{"~" * self.level}<li>If your registration is cancelled earlier than 30 days before your date of arrival, your payment will be refunded in full except for a R50 admin fee.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>Cancellations later than 30 days before your date of arrival will not be refunded as the full expenses will already have been incurred for the registration.</li>'
        )
        self.level -= 1
        self.out.append(f'{"~" * self.level}</ul>')

    def close(self):
        self.out.append(f'{"~" * self.level}<center>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<button type="submit">Submit Registration Form</button>'
        )
        self.level -= 1
        self.out.append(f'{"~" * self.level}</center>')
        self.level -= 1
        self.out.append("</form>")

    def render(self):
        return "\n".join([l.replace("~", "   ") for l in self.out])

    def open_containing_div(self, cls=None):
        if cls:
            cls_div = f' id="{cls}_div"'
            cls_fs = f' id="{cls}_fs"'
        else:
            cls_div = ""
            cls_fs = ""
        self.out.append(f"{'~' * self.level}<div{cls_div}>")
        self.level += 1
        self.out.append(f"{'~' * self.level}<fieldset{cls_fs} disabled>")
        self.level += 1

    def close_containing_div(self):
        self.level -= 1
        self.out.append(f"{'~' * self.level}</fieldset>")
        self.level -= 1
        self.out.append(f"{'~' * self.level}</div>")

    def open_form_item(self, tag, label, number=False):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<label for="{tag}" class="col-sm-{"8" if number else "4"} col-form-label">'
        )
        self.level += 1
        self.out.append(f'{"~" * self.level}{label}: ')
        self.level -= 1
        self.out.extend(
            (
                f'{"~" * self.level}</label>',
                f'{"~" * self.level}<div class="col-sm-{"4" if number else "8"}">',
            )
        )
        self.level += 1

    def close_form_item(self):
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_header(self, text):
        self.out.append(f'{"~" * self.level}<h2>{text}</h2>')

    def add_label(self, tag, text, centre=False, font=None):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        if centre:
            self.out.append(f'{"~" * self.level}<center>')
            self.level += 1
        self.out.append(
            f'{"~" * self.level}<label id={tag}>{"<h3>" if font=="b" else ""}{text}{"</h3>" if font=="b" else ""}</label>'
        )
        self.level -= 1
        if centre:
            self.out.append(f'{"~" * self.level}</center>')
            self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_text(
        self,
        tag,
        label,
        help="",
        type="text",
        cls="",
        cost=None,
        disabled=False,
    ):
        if label:
            self.open_form_item(tag, label, number=type == "number")
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        if cost:
            cost_attr = f" cost={cost}"
        else:
            cost_attr = ""
        base_class = "form-control"
        if cls:
            class_attr = f"{base_class} {cls}"
        else:
            class_attr = base_class
        m = ""
        if type == "number":
            m = 'min="0" value="0"'
        inner = [
            f'{"~" * self.level}<input type="{type}" {m} class="{class_attr}" id="{tag}" name="{tag}"{help_attr}{cost_attr}{" disabled" if disabled else ""}>'
        ]
        if help:
            inner.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            inner.append(f'{"~" * self.level}{help}')
            self.level -= 1
            inner.append(f'{"~" * self.level}</small>')
        self.out.extend(inner)
        if label:
            self.close_form_item()

    def add_email(self, tag, label, help=""):
        self.add_text(tag, label, help=help, type="email")

    def add_checkbox(self, tag, label, help=""):
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        self.out.append(f'{"~" * self.level}<div class="form-check">')
        self.level += 1
        self.out.extend(
            (
                f'{"~" * self.level}<input class="form-check-input" type="checkbox" value="{tag}" name="{tag}" id="{tag}"{help_attr}>',
                f'{"~" * self.level}<label class="form-check-label" for="{tag}">',
            )
        )
        self.level += 1
        self.out.append(f'{"~" * self.level}{label}')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</label>')
        if help:
            self.out.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            self.out.append(f'{"~" * self.level}{help}')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</small>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_selector(self, tag, label, items, help="", dummy_text="", required=False):
        """If dummy_text, add the text as the first entry"""
        self.open_form_item(tag, label)
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        inner = [
            f'{"~" * self.level}<select class="form-control" id="{tag}" name="{tag}"{help_attr}{" required" if required else ""}>'
        ]
        self.level += 1
        if dummy_text:
            inner.append(f'{"~" * self.level}<option value="">{dummy_text}</option>')
        inner.extend(
            [
                f'{"~" * self.level}<option value="{item}">{item}</option>'
                for item in items
            ]
        )
        self.level -= 1
        inner.append(f'{"~" * self.level}</select>')
        if help:
            inner.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            inner.append(f'{"~" * self.level}{help}')
            self.level -= 1
            inner.append(f'{"~" * self.level}</small>')
        self.out.extend(inner)
        self.close_form_item()

    def add_radios(self, name, options):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        for (n, (k, v)) in enumerate(options.items()):
            self.out.append(f'{"~" * self.level}<div class="form-check">')
            self.level += 1
            self.out.append(
                f'{"~" * self.level}<input class="form-check-input" type="radio" name="{name}" id="{k}" value="{k}"{" checked" if n == 0 else ""}>'
            )
            self.out.append(
                f'{"~" * self.level}<label class="form-check-label" for="{k}">'
            )
            self.level += 1
            self.out.append(f'{"~" * self.level}{v}')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</label>')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</div>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_divider(self):
        self.out.append(f'{"~" * self.level}<hr>')


def make_attendee_fields(html, prefix, lion=True, required=False):
    html.add_text(
        f"{prefix}_first_names",
        "First Name(s)",
        "Attendee's first name or names. Please use a real name rather than a nickname - nicknames can however be used for the name badge later in this form.",
    )
    html.add_text(f"{prefix}_last_name", "Last Name")

    html.add_text(
        f"{prefix}_name_badge",
        "Name Badge",
        help=f"The name to appear on the attendee's name badge. eg {'Joe Bloggs; Lion John Doe, ZC Wendy Bloggs, PDG Jane Doe' if lion else 'Joe Bloggs, Partner in Service Jane Doe'}",
    )

    if lion:
        html.add_selector(
            f"{prefix}_club",
            "Lions Club",
            CLUBS,
            dummy_text="Please select a club",
            required=required,
        )
    html.add_text(
        f"{prefix}_cell",
        "Cell Phone",
        "A cellphone number the attendee can be reached at if needed. This number may also be used for urgent SMSes for changes of plan during the convention.",
    )
    html.add_email(f"{prefix}_email", "Email Address")
    html.add_text(
        f"{prefix}_dietary",
        "Dietary Requirements",
        help="Please be VERY clear with these requirements. eg halal, kosher, vegetarian, allergic to dairy",
    )
    html.add_text(
        f"{prefix}_disability",
        "Special Access Requirements",
        help="Please indicate any requirements for wheel chair access or the like, if applicable",
    )

    html.add_checkbox(
        f"{prefix}_mjf_lunch",
        "Attendee will attend the Melvin Jones Lunch.",
        help="This lunch is only open to Melvin Jones Fellows and may carry an extra cost. Details will be provided closer to the time.",
    )

    html.add_checkbox(
        f"{prefix}_pdg_dinner",
        "Attendee will attend the PDGs Breakfast.",
        help="This event is only open to PDGs and their partners and may carry an extra cost. Details will be provided closer to the time.",
    )

    html.add_checkbox(
        f"{prefix}_beach_cleanup",
        "Attendee will attend the Beach Cleanup service project.",
        help="Details of this project will be provided closer to the time.",
    )

    if lion:
        html.add_checkbox(
            f"{prefix}_lpe_breakfast",
            "Attendee will attend the President's Elect Breakfast.",
            help="A gathering for Lions who will serve as club president in the 2023/24 Lionistic year, paid for by MD410. Details will be provided closer to the time.",
        )

    if not lion:
        html.add_checkbox(
            f"{prefix}_partner_program",
            "Attendee would be interested in the partner's program.",
            help="The partner's program may involve an additional cost. Details will be provided closer to the time.",
        )


html = HTML()
html.add_header("First Attendee")
make_attendee_fields(html, "main", required=True)
html.add_divider()
html.add_radios(
    "partner",
    {
        "partner_none": "No partner will be coming with me",
        "partner_lion": "My Lion partner will be coming with me",
        "partner_non_lion": "My non-Lion partner in service will be coming with me",
    },
)
html.open_containing_div(cls="partner_lion")
html.add_header("Lion Partner")
make_attendee_fields(html, "partner_lion", required=False)
html.close_containing_div()
html.open_containing_div(cls="partner_non_lion")
html.add_header("Non Lion Partner")
make_attendee_fields(html, "partner_non_lion", lion=False, required=False)
html.close_containing_div()
html.add_divider()
# html.add_header("Registrations")
# html.add_radios(
#     "reg_type",
#     {
#         "full_reg": "I will be making full registrations",
#         "partial_reg": "I will be making one or more partial registrations",
#     },
# )
# html.open_containing_div(cls="full_reg")
# html.add_header("Full Registrations")
# html.add_text(
#     "full_reg",
#     "Number of Full Registrations (R1285 per person)",
#     help="Full registration includes <ul><li>Welcome Evening</li><li>Lunch and teas during District and MD Conventions</li><li>Banquet</li><li>Theme Evening</li></ul>",
#     type="number",
#     cls="total",
#     cost=1285,
# )
# html.close_containing_div()
# html.open_containing_div(cls="partial_reg")
# html.add_header("Partial Registrations")
# html.add_text(
#     "partial_reg_banquet",
#     '<a href="/events/banquet">Number of Banquet Registrations</a> (R500 per person)',
#     type="number",
#     cls="total",
#     cost=500,
# )
# html.add_text(
#     "partial_reg_convention",
#     '<a href="/events/md_convention">Number of MD410 Convention Registrations</a> (R400 per person)',
#     help="Includes lunch and teas",
#     type="number",
#     cls="total",
#     cost=400,
# )
# html.add_text(
#     "partial_reg_theme",
#     '<a href="/events/theme_evening">Number of Theme Evening Registrations</a> (R450 per person)',
#     type="number",
#     cls="total",
#     cost=450,
# )
# html.close_containing_div()
html.add_header("Convention Pins")
html.add_text(
    "pins",
    "Total Number of Convention Pins (R60 per pin)",
    type="number",
    cls="total",
    cost=60,
)
html.add_divider()
html.add_label("total_cost", "Total Cost: R1300", centre=True, font="b")
html.close()

with open("../../content/registration/_index.html", "w") as fh:
    fh.write(html.render())
print(html.render())

"Something Fishy" theme evening
23/04/29
19:00-
[Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)

The last evening of the MD Convention, a theme evening with the theme "Something Fishy". Clubs and members are encouraged to dress to the theme.

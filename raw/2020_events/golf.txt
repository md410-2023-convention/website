Golf
20/05/01
07:00-
TBC

For the golfing enthusiasts who will already be in Durban, Lion Perry Chetty will be organising a round of golf on the morning of Friday 1 May. Please contact the registration secretary for Lion Perry's contact details if you'd like to get in a round. 

**This event will be for your own cost and is not covered by the Convention registration fee.**

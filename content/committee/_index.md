---
title: "Convention Committee"
date: 2019-07-14T16:19:07+02:00
draft: false
---

The 2023 MD410 Oceans of Opportunity Convention will be convened by a team of dedicated and passionate Lions from several clubs in East London:

* **Convention Convenor**: PDG Ivan Piater
    * (+27) 083 206 7735
    * [portrexlionsclub68@gmail.com](mailto:portrexlionsclub68@gmail.com)
* **Secretary**: Shireen Piater
    * (+27) 083 396 5260
    * [portrexlionsclub68@gmail.com](mailto:portrexlionsclub68@gmail.com)
* **Treasurer**: Pat van der Merwe
    * [portrexlionsclub68@gmail.com](mailto:portrexlionsclub68@gmail.com)
* **Online Registrations**: PDG Kim van Wyk
    * 083 384 4260
    * [vanwykk@gmail.com](mailto:vanwykk@gmail.com)
    * Please email or WhatsApp where possible


The MD410 Standing Committee on Conventions advises the convention committee and the MD410 Council on the Convention:

* **Standing Committee on Conventions Chair**: PCC Mike Newlands [mikenewlands@border.co.za](mailto:mikenewlands@border.co.za)

Please direct queries to PDG Ivan Piater on (+27) 083 206 7735 or [portrexlionsclub68@gmail.com](mailto:portrexlionsclub68@gmail.com)

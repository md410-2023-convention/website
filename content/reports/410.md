---
title: Multiple District 410 Convention Reports
draft: false
---

The reports for the Multiple District 410 Convention can be downloaded from this page. All of the reports can be downloaded as a single document or each report can be individually downloaded. In the interest of reducing the length of the combined report, the audited MD and Brightsight financial reports from 2019/20, 2020/21 and 2021/22 are not included in the combined report but can be individually downloaded below.

* [Click here to download the combined MD 410 convention reports (excluding audit reports for previous years)](/docs/410_reports/230429_md_convention_combined_reports.pdf)

## Audited Financial Reports from Previous Years

* [2019/20 audit reports](/docs/410_reports/audited_financials/230429_15_b_1_audited_accounts_year_ending_2020.pdf)
* [2020/21 audit reports](/docs/410_reports/audited_financials/230429_15_b_2_audited_accounts_year_ending_2021.pdf)
* [2021/22 audit reports](/docs/410_reports/audited_financials/230429_15_b_3_audited_accounts_year_ending_2022.pdf)

## Audited Brightsight Reports from Previous Years

* [2019/20 audit reports](/docs/410_reports/brightsight_financials/230429_16_1_lions_brightsight_audited_accounts_year_ending_2020.pdf)
* [2020/21 audit reports](/docs/410_reports/brightsight_financials/230429_16_2_lions_brightsight_audited_accounts_year_ending_2021.pdf)
* [2021/22 audit reports](/docs/410_reports/brightsight_financials/230429_16_3_lions_brightsight_audited_accounts_year_ending_2022.pdf)

## Individual Convention Reports

* [Agenda](/docs/410_reports/reports/230429_00_agenda.pdf)
* [Invocation](/docs/410_reports/reports/230429_03_invocation.pdf)
* [National Anthems](/docs/410_reports/reports/230429_04_anthems.pdf)
* [Lions Pledge](/docs/410_reports/reports/230429_05_pledge.pdf)
* [Lions Code of Ethics](/docs/410_reports/reports/230429_06_code_of_ethics.pdf)
* [Minutes Of The 58th Convention Held In East London - 30 April 2022](/docs/410_reports/reports/230429_12_2022_convention_minutes.pdf)
* [Report by District Governor - District 410W](/docs/410_reports/reports/230429_14_1_district_410w.pdf)
* [Report by District Governor - District 410E](/docs/410_reports/reports/230429_14_2_district_410e.pdf)
* [Finance Committee](/docs/410_reports/reports/230429_15_a_finance_committee.pdf)
* [Interim Accounts for the period ended 31 March 2023](/docs/410_reports/reports/230429_15_b_4_interim_accounts_to_31_march_2023.pdf)
* [MD410 Budget 2023 - 2024](/docs/410_reports/reports/230429_15_b_5_budget.pdf)
* [Interim Accounts for the period ended 31 March 2023](/docs/410_reports/reports/230429_16_4_lions_brightsight_interim_accounts_31_march_2023.pdf)
* [Motions for Resolution](/docs/410_reports/reports/230429_17_motions_for_resolution.pdf)
* [Global Membership Team](/docs/410_reports/reports/230429_19_1_gmt.pdf)
* [Global Leadership Team](/docs/410_reports/reports/230429_19_2_1_glt.pdf)
* [Vision 2022 and Beyond](/docs/410_reports/reports/230429_19_2_2_vision_2022_and_beyond.pdf)
* [Global Service Team](/docs/410_reports/reports/230429_19_3_gst.pdf)
* [LCIF](/docs/410_reports/reports/230429_20_1_lcif.pdf)
* [Peace Poster & Essay Contest](/docs/410_reports/reports/230429_20_2_peace_poster_and_essay_contest.pdf)
* [Youth / Youth Exchange](/docs/410_reports/reports/230429_20_3_youth_camp_and_exchange.pdf)
* [Information Technology](/docs/410_reports/reports/230429_20_4_it.pdf)
* [Disaster Relief](/docs/410_reports/reports/230429_20_5_disaster_relief.pdf)
* [Christmas Cakes](/docs/410_reports/reports/230429_20_6_a_christmas_cakes.pdf)
* [Christmas Cake Pricing](/docs/410_reports/reports/230429_20_6_b_cake_pricing.xlsx)
* [Leos](/docs/410_reports/reports/230429_20_7_leos.pdf)
* [2023 MD Convention Convenor Report](/docs/410_reports/reports/230429_22_1_2023_convention.pdf)
* [2024 MD Convention Convenor Report](/docs/410_reports/reports/230429_22_3_2024_convention.pdf)

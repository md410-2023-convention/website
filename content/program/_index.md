---
title: "Lions MD410 2023 Convention Program"
draft: false
---

The Lions MD410 2023 Oceans of Opportunity Convention will be held from

**Thursday 27 April to Sunday 30 April 2023**. 

Some Lions serving in District and Multiple District portfolios will be attending events during the day on Thursday 27 April but most Lions will only need to arrive in time for the [Welcome Evening](/events/welcome_evening).

Some events will be held at the the Golden Hours School, which  is leess than 5 minutes travel from the Riverside Hotel.

## Wednesday 26 April 2023

Time | Event (click on event for further details) | Venue (click for map to venue)
 ---|---  |---
18:30- | [Outgoing and Incoming Council dinner](/events/council_dinner) | [Riverside Hotel Restaurant](/venue)

## Thursday 27 April 2023

Time | Event (click on event for further details) | Venue (click for map to venue)
 ---|---  |---
08:30-12:30 | [MD410 4th Council meeting](/events/council_meeting) | Golden Hours Schoo
12:30-13:30 | [Council meeting lunch](/events/council_meeting_lunch) | Golden Hours Schoo
13:30-16:30 | [District 410E Cabinet meetings](/events/cabinet_meeting_e) | Golden Hours School
13:30-16:30 | [District 410W Cabinet meetings](/events/cabinet_meeting_w) | Golden Hours School
18:30-22:00 | [Welcome Evening](/events/welcome_evening) | [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)

## Friday 28 April 2023

Time | Event (click on event for further details) | Venue (click for map to venue)
 ---|---  |---
07:00-08:30 | [PDGs breakfast](/events/pdgs_function) | [Riverside Hotel Restaurant](/venue)
08:30-12:30 | [District 410E Convention](/events/district_e_convention) | [Riverside Hotel Conference Centre](/venue)
08:30-12:30 | [District 410W Convention](/events/district_w_convention) | [Riverside Hotel Conference Centre](/venue)
12:30-13:30 | [Convention delegates lunch - own expense](/events/district_convention_lunch) | 
14:00-16:30 | [Service Project - Beach Cleanup and Mangroves Tour](/events/service_project_beach) | 
18:15-18:45 | [Pre-banquet drinks](/events/banquet_drinks) | [Riverside Hotel](/venue)
19:00-22:00 | [Banquet](/events/banquet) | [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)
22:30- | [Post-banquet coffee and nightcap](/events/banquet_nightcap) | [Poolside - Riverside Hotel](/venue)

## Saturday 29 April 2023

Time | Event (click on event for further details) | Venue (click for map to venue)
 ---|---  |---
07:00-08:30 | [Breakfast at the hotel](/events/breakfast_saturday) | [Riverside Hotel](/venue)
07:00-08:30 | [Presidents Elects Breakfast](/events/presidents_breakfast) | [Riverside Hotel Restaurant](/venue)
09:00-15:00 | [MD410 Convention](/events/md_convention) | [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)
12:00-13:30 | [Convention delegates lunch](/events/md_convention_lunch) | [Riverside Hotel Conference Centre](/venue)
12:00-13:30 | [Melvin Jones Fellows lunch](/events/mjf_lunch) | [Pelican Suite 4 - Riverside Hotel Conference Centre](/venue)
19:00- | ["Something Fishy" theme evening](/events/theme_evening) | [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)

## Sunday 30 April 2023

Time | Event (click on event for further details) | Venue (click for map to venue)
 ---|---  |---
07:00-09:00 | [Breakfast at the hotel](/events/breakfast_sunday) | [Riverside Hotel](/venue)
09:00- | [Golf](/events/golf) | 

---
title: "Lions MD410 2023 Convention"
date: 2019-07-14T16:19:07+02:00
draft: false
---

<center>
<h1>Multiple District 410</h1>
<h1>OCEANS OF OPPORTUNITY</h1>
<h1>Convention 2023</h1>
</center>

Welcome to the 2023 [Lions MD410](https://www.lionsclubs.co.za) Oceans of Opportunity Convention website, to be held from <font style="color:#E1CC00;font-size:120%">27-30 April 2023</font> in Durban.

[Click here](registration) to register for the full Lions MD410 2023 Convention.

[Click here](partial_registration) to register for one or more individual events at the Lions MD410 2023 Convention.

If you have any questions:
* contact the convenor Trevor Hobbs on liontrevorhobbs@gmail.com or 076 731 2993
* contact the registration secretary Kim van Wyk on vanwykk+mdc2023@gmail.com or 083 384 4260 (preferably by WhatsApp or SMS rather than voice call if possible)

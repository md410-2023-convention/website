---
title: "Lions MD410 2023 Convention Accommodation"
draft: false
---

The Lions MD410 2023 Oceans of Opportunity Convention will be held at the [Riverside Hotel](https://theriversidehotel.co.za/), nestled on the mouth of the Umgeni River in Durban North. The hotel is located 20 minutes from the King Shaka airport and is easily reached via major highways. Plentiful accommodation is available at the Riverside Hotel.

Accommodation bookings at the Riverside Hotel should be made directly with the hotel - the MD410 2023 Convention committee are not handling accommodation requirements. 

The hotel can be contacted on +27 (0) 31 563 0600 or at [reservations@riversidehotel.co.za](mailto:reservations@riversidehotel.co.za). Full contact details for the hotel are on their [website](https://theriversidehotel.co.za/). 

<center><h4 style="color:yellow;">When booking at the hotel, make sure to inform them that you're booking for the Lions Convention by quoting code <em>LIONS2023</em>.</h4></center>

## Rates and Rooms

The Riverside Hotel has offered a substantially reduced rate for the MD Convention, which are only valid if you book directly with the hotel. These rates include VAT and include breakfast.

### Standard Room (85 available)
<div class="text-center">
    <img src="/img/riverside_room_standard.png" width="300" alt="Riverside Hotel - Standard Room" class="rounded img-fluid">
</div>

<ul>
  <li>Double rate: R1565 per room per night (R785.50 per person sharing per night)</li>
  <li>Single rate: R1395 per room per night</li>
</ul>

### Deluxe Room (12 available)
<div class="text-center">
    <img src="/img/riverside_room_deluxe.png" width="300" alt="Riverside Hotel - Deluxe Room" class="rounded img-fluid">
</div>

<ul>
  <li>Double rate: R1765 per room per night (R882.50 per person sharing per night)</li>
  <li>Single rate: R1595 per room per night</li>
</ul>

### Family Room (2 double beds or 1 double bed and 2 single beds - 24 available)

<div class="text-center">
    <img src="/img/riverside_room_family.png" width="300" alt="Riverside Hotel - Family Room" class="rounded img-fluid">
</div>

<ul>
  <li>Rate: R2400 per room per night</li>
</ul>

## Further Details

See the [Venue](/venue) page for details on the Riverside Hotel's location and directions.

## Alternative Accommodation

There are a number of B&B's and small hotels in the immediate area and several B&B's and larger hotels in Umhlanga are only 10-15 minutes away.

---
title: 'Breakfast at the hotel'
draft: false
---

**This event is not covered by the Convention registration and is an additional cost for the attendees.**
\
\
**Date and Time**: Sunday 30 April 2023, 07:00-09:00 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)

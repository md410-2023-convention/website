---
title: 'Welcome Evening'
draft: false
---

An informal social gathering to welcome attendees to the Convention, including dinner.
\
\
**Date and Time**: Thursday 27 April 2023, 18:30-22:00 \
**Location**: [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

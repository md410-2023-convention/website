---
title: 'Convention delegates lunch - own expense'
draft: false
---

Lunch for delegates to the 2021 MD410W and MD410E District Conventions. **This meal is not covered by the Convention registration and is an additional cost for the attendees.**
\
\
**Date and Time**: Friday 28 April 2023, 12:30-13:30 \
**Location**: 
\
\
[Back to Program](/program)

---
title: 'MD410 4th Council meeting'
draft: false
---

The 4th MD410 Council meeting.
\
\
**Date and Time**: Thursday 27 April 2023, 08:30-12:30 \
**Location**: Golden Hours School
\
\
[Back to Program](/program)

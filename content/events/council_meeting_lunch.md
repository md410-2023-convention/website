---
title: 'Council meeting lunch'
draft: false
---

Lunch for the Lions and guests attending the MD410 Council meeting. Please ensure you have advised the Registration Secretary or Convention Convenor of any dietary requirements.
\
\
**Date and Time**: Thursday 27 April 2023, 12:30-13:30 \
**Location**: Golden Hours School
\
\
[Back to Program](/program)

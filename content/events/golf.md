---
title: 'Golf'
draft: false
---

A round of golf with fellow Lions before heading home. **This event is not covered by the Convention registration and is an additional cost for the attendees.**
\
\
**Date and Time**: Sunday 30 April 2023, 09:00- \
**Location**: 
\
\
[Back to Program](/program)

---
title: 'PDGs breakfast'
draft: false
---

A function for all attending PDGs and the current DGs to welcome and congratulate the DGs. **This event is not covered by the Convention registration and is an additional cost for the attendees.**

Please indicate on your registration form if you will be attending the PDGs function, or contact the Registration Secretary Kim van Wyk if you have already registered for the Convention and wish to attend the PDGs function.
\
\
**Date and Time**: Friday 28 April 2023, 07:00-08:30 \
**Location**: [Riverside Hotel Restaurant](/venue)
\
\
[Back to Program](/program)

---
title: 'Presidents Elects Breakfast'
draft: false
---

Breakfast and discussion for incoming club presidents with the Council Chair.
\
\
**Date and Time**: Saturday 29 April 2023, 07:00-08:30 \
**Location**: [Riverside Hotel Restaurant](/venue)
\
\
[Back to Program](/program)

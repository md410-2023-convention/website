---
title: 'MD410 Convention'
draft: false
---

The 2023 Multiple District 410 Convention.
\
\
**Date and Time**: Saturday 29 April 2023, 09:00-15:00 \
**Location**: [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

---
title: 'Breakfast at the hotel'
draft: false
---

**This event is not covered by the Convention registration and is an additional cost for the attendees.**
\
\
**Date and Time**: Saturday 29 April 2023, 07:00-08:30 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)

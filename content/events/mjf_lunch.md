---
title: 'Melvin Jones Fellows lunch'
draft: false
---

The traditional lunch for Melvin Jones Fellows. **This event is not covered by the Convention registration and is an additional cost for the attendees.**
\
\
**Date and Time**: Saturday 29 April 2023, 12:00-13:30 \
**Location**: [Pelican Suite 4 - Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

---
title: 'Banquet'
draft: false
---

A formal, black-tie dinner event, celebrating Lions achievements during the previous year.
\
\
**Date and Time**: Friday 28 April 2023, 19:00-22:00 \
**Location**: [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

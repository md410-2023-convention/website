---
title: 'District 410W Convention'
draft: false
---

The 2023 District 410W Conventions.
\
\
**Date and Time**: Friday 28 April 2023, 08:30-12:30 \
**Location**: [Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

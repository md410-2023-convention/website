---
title: 'District 410E Convention'
draft: false
---

The 2023 District 410E Conventions.
\
\
**Date and Time**: Friday 28 April 2023, 08:30-12:30 \
**Location**: [Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

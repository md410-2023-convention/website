---
title: 'Convention delegates lunch'
draft: false
---

Lunch for delegates to the 2023 MD410 Convention.
\
\
**Date and Time**: Saturday 29 April 2023, 12:00-13:30 \
**Location**: [Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

---
title: 'Post-banquet coffee and nightcap'
draft: false
---

An informal social gathering after the banquet, enjoying a cup of coffee or a nightcap with your fellow attendees.
\
\
**Date and Time**: Friday 28 April 2023, 22:30- \
**Location**: [Poolside - Riverside Hotel](/venue)
\
\
[Back to Program](/program)

---
title: 'Outgoing and Incoming Council dinner'
draft: false
---

The 2022/23 and 2023/24 Councils of Governors will meet for dinner.
\
\
**Date and Time**: Wednesday 26 April 2023, 18:30- \
**Location**: [Riverside Hotel Restaurant](/venue)
\
\
[Back to Program](/program)

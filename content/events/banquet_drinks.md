---
title: 'Pre-banquet drinks'
draft: false
---

A gathering before the banquet for PDGs, the current Council, newly elected DGEs, 1VDGEs and 2VDGEs and invited guests.
\
\
**Date and Time**: Friday 28 April 2023, 18:15-18:45 \
**Location**: [Riverside Hotel](/venue)
\
\
[Back to Program](/program)

---
title: 'Service Project - Beach Cleanup and Mangroves Tour'
draft: false
---

An opportunity to participate in a service project, cleaning up the Blue Lagoon beach area, about 5 minutes travel from the Riverside Hotel, followed by a tour of the unique Umgeni River Mangroves, also very close to the hotel.
\
\
**Date and Time**: Friday 28 April 2023, 14:00-16:30 \
**Location**: 
\
\
[Back to Program](/program)

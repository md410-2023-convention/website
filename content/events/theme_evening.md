---
title: '"Something Fishy" theme evening'
draft: false
---

The last evening of the MD Convention, a theme evening with the theme "Something Fishy". Clubs and members are encouraged to dress to the theme.
\
\
**Date and Time**: Saturday 29 April 2023, 19:00- \
**Location**: [Pelican Suite 1-3 - Riverside Hotel Conference Centre](/venue)
\
\
[Back to Program](/program)

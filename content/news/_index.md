---
title: "News and Newsletters"
date: 2019-07-14T16:19:07+02:00
draft: false
---

<h2>Newsletter 6</h2>
<div class="text-center">
    <img src="/img/newsletter_06.png" width="600" alt="Newsletter 06" class="rounded img-fluid">
</div>
<a href="/docs/newsletter_06.pdf">Download as a PDF</a>

<h2>Newsletter 5</h2>
<div class="text-center">
    <img src="/img/newsletter_05.png" width="600" alt="Newsletter 05" class="rounded img-fluid">
</div>
<a href="/docs/newsletter_05.pdf">Download as a PDF</a>

<h2>Newsletter 4</h2>
<div class="text-center">
    <img src="/img/newsletter_04.png" width="600" alt="Newsletter 04" class="rounded img-fluid">
</div>
<a href="/docs/newsletter_04.pdf">Download as a PDF</a>

<h2>Newsletter 3</h2>
<div class="text-center">
    <img src="/img/newsletter_03.png" width="600" alt="Newsletter 03" class="rounded img-fluid">
</div>
<a href="/docs/newsletter_03.pdf">Download as a PDF</a>

<h2>Newsletter 2</h2>
<div class="text-center">
    <img src="/img/newsletter_02.png" width="600" alt="Newsletter 02" class="rounded img-fluid">
</div>
<a href="/docs/newsletter_02.pdf">Download as a PDF</a>

<h2>Newsletter 1</h2>
<div class="text-center">
    <img src="/img/newsletter_01.png" width="600" alt="Newsletter 01" class="rounded img-fluid">
</div>
<a href="/docs/newsletter_01.pdf">Download as a PDF</a>

---
title: "International Guest"
date: 2019-07-14T16:19:07+02:00
draft: false
---

We will be joined at our MD410 Convention by an esteemed international guest: International Director Ahmed Salem Mostafa Salem. 
<div class="text-center">
    <img src="/img/int_guest.jpg" alt="Portrait of ID Ahmed Salem Mostafa Salem" class="rounded img-fluid">
</div>


Dr. Engineer Ahmed Salem Mostafa Salem from Cairo, Egypt was elected to serve a two-year term as a director of Lions Clubs International at the association’s 104th International Convention, June 24 through June 28, 2022.

Director Salem has more than 56 years of consultation experience in the field of construction engineering. He is vice chairman and CEO of SIAC Industrial Construction & Engineering Company. Under his leadership SIAC has undertaken numerous high-profile projects including several towers and bank headquarters within the New Administrative Capital. He has also successfully completed a number of mega projects throughout Saudi Arabia, Kuwait and Qatar. He joined the Cairo North Lions Club in 1995 to be a part of the unique service and leadership opportunities that the association offers worldwide. He has held a number of offices within the association including being a member of the Africa Lions steering committee, the Africa Lions Think Tank Group, GMT & GAT area leader, and the Future Africa Headquarter Committee. He also served as head of the construction committee for the Lions Eye Hospital in Ashmoun City, Egypt.

In recognition of his service to the association, he has received numerous awards including multiple International President’s Awards and an International President’s Leadership Award. He is also a Progressive Melvin Jones Fellow and major donor LCIF Campaign 100. 

In addition to his Lions activities, Director Salem is active in numerous professional and community organizations and is a member of the Engineering Syndicate and the Society of Civil Engineers.

Director Salem and his spouse, Hanem Sahmoud, who is also a Lion and Melvin Jones Fellow, have four children and eight grandchildren.

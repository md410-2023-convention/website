$(document).ready(function() {
  $('.total').change(function () {
    var sum = 0;
    $('.total').each(function() {
      sum += Number($(this).val()) * Number($(this).attr("cost"));
    });
    $('#total_cost').html("<h3>Total Cost: R" + sum + "</h3>");
  });
});
